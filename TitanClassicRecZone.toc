## Interface: 11305
## Title: Titan Panel Classic [|cffeda55fRecomended Zone|r] |cff00aa001.0.0.6|r
## Notes: Displays level for current zone and recommends zones and instances for your level.
## Version: 1.0.0.6
## X-Date: 2020-10-30
## Author: Kernighan
## SavedVariables: 
## SavedVariablesPerCharacter: 
## OptionalDeps: 
## Dependencies: TitanClassic
Libs.xml
TitanClassicRecZone.xml
